package clock;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;


/**
 * An abstract class that draws a clock to be drawn however the subclass chooses.
 * 
 * @author Charles Cusack, October 2001. Modified January, 2013 (Removed ALibrary stuff, refactored).
 */
public abstract class ClockView extends JPanel {
	private ClockInterface	clock;

	public ClockView(int width, int height, ClockInterface clock) {
		super();
		setPreferredSize(new Dimension(width, height));
		this.clock = clock;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawFace(g);
		drawSeconds(g);
		drawMinutes(g);
		drawHours(g);
	}

	/*
	 * Delegate methods.
	 */
	public int getSeconds() {
		return clock.getSeconds();
	}

	public int getMinutes() {
		return clock.getMinutes();
	}

	public int getHours() {
		return clock.getHours();
	}

	/*
	 * The abstract methods that subclasses must implement to properly draw the clock.
	 */

	/**
	 * Draw the face of the clock. This will be drawn first.
	 * 
	 * @param g The Graphics object to draw on.
	 */
	protected abstract void drawFace(Graphics g);

	/**
	 * Draw the current seconds on the clock. This will be drawn after face and before minutes.
	 * 
	 * @param g The Graphics object to draw on.
	 */
	protected abstract void drawSeconds(Graphics g);

	/**
	 * Draw the current minutes on the clock. This will be drawn after seconds and before hours.
	 * 
	 * @param g The Graphics object to draw on.
	 */
	protected abstract void drawMinutes(Graphics g);

	/**
	 * Draw the current hours on the clock. This will be drawn after minutes.
	 * 
	 * @param g The Graphics object to draw on.
	 */
	protected abstract void drawHours(Graphics g);
	// ---------------------------------------------------------------------------
}
